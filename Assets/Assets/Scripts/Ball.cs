﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ball : MonoBehaviour {
    public float speed = 10;
    public int scoreLeft = 0;
    public int scoreRight = 0;
    public Text scoreLeftText;
    public Text scoreRightText;
    public Text victoryText;
    public AudioClip victory;


    //Last score. 0 = left. 1 = right. -1 = newGame (no one has scored yet).
    public int lastScorer = -1;

	// Use this for initialization
	void Start () {
        GetComponent<AudioSource>().playOnAwake = false;
        GetComponent<AudioSource>().clip = victory;
        int direction = Random.Range(0, 2);
        if (direction == 0)
            GetComponent<Rigidbody2D>().velocity = Vector2.left * speed;
        else if(direction == 1)
            GetComponent<Rigidbody2D>().velocity = Vector2.right * speed;
    }

    void BallRestart ()
    {
        GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        gameObject.transform.position = new Vector2(0,0);
        StartCoroutine(Wait());
    }

    void Restart()
    { 
        if (lastScorer == 0)
            GetComponent<Rigidbody2D>().velocity = Vector2.left * speed;
        else if (lastScorer == 1)
            GetComponent<Rigidbody2D>().velocity = Vector2.right * speed;
    }

    // We will use this to detect collision with the paddle.
    void OnCollisionEnter2D(Collision2D col) {
        
        // Did it hit the left paddle?
        if (col.gameObject.name == "PaddleLeft")
        {
            // Get the hit factor
            float y = hitFactor(transform.position, col.transform.position, col.collider.bounds.size.y);
            // Let's normalize the hitfactor as Vector 2
            Vector2 dir = new Vector2(1, y).normalized;
            // Set velocity with dir * speed
            GetComponent<Rigidbody2D>().velocity = dir * speed;

        }
        // Did it hit the right paddle?
        if (col.gameObject.name == "PaddleRight")
        {
            // Get the hit factor
            float y = hitFactor(transform.position, col.transform.position, col.collider.bounds.size.y);
            // Let's normalize the hitfactor as Vector 2
            Vector2 dir = new Vector2(-1, y).normalized;
            // Set velocity with dir * speed
            GetComponent<Rigidbody2D>().velocity = dir * speed;
        }

        if (col.gameObject.name == "ScoreColliderLeft")
        {
            scoreRight++;
            lastScorer = 0;
            scoreRightText.text = scoreRight.ToString();
            // Is the score at 10?
            if (scoreRight >= 10)
            {
                victoryText.text = "Player 2 Wins!";
                GetComponent<AudioSource>().Play();
                Destroy(gameObject);
            }
            else
            {
                BallRestart();
            }
        }
        // Did it hit the right paddle?
        if (col.gameObject.name == "ScoreColliderRight")
        {
            scoreLeft++;
            lastScorer = 1;
            scoreLeftText.text = scoreLeft.ToString();
            // Is the score at 10?
            if (scoreLeft >= 10)
            {
                victoryText.text = "Player 1 Wins!";
                GetComponent<AudioSource>().Play();
                Destroy(gameObject);
            }
            else
            {
                // If not a victory, reset the ball
                BallRestart();
            }
        }

    }

    float hitFactor(Vector2 ballPos, Vector2 paddlePos, float paddleHeight) {
        return (ballPos.y - paddlePos.y) / paddleHeight;
    }

    IEnumerator Wait()
    {
        yield return new WaitForSeconds(5);
        Restart();
    }

}
