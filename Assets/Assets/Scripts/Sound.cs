﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sound : MonoBehaviour
{
    public AudioClip sound;
    public string targetTag;

    public void OnCollisionEnter2D(Collision2D collided)
    {
        if(collided.gameObject.tag == targetTag)
        {
            AudioSource.PlayClipAtPoint(sound, Camera.main.transform.position, 1);
        }
        Debug.Log("AHHHHHH");
    }
}
